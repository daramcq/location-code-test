import tornado.web
import tornado.ioloop
from app.views import (
    CityView,
    ComparisonView
)


def make_app():
    return tornado.web.Application([
        (r"/city/([a-zA-Z0-9]*)?", CityView, {}),
        (r"/comparison/cities=\[(\S+)\]&profile=(\S+)", ComparisonView, {})
    ], debug=True)

if __name__ == "__main__":
    application = make_app()
    application.listen(8484)
    tornado.ioloop.IOLoop.current().start()
