import numbers
import os
from tornado.testing import AsyncHTTPTestCase
from tornado.escape import json_decode
from tornado.web import Application
from http import HTTPStatus
from app.views import CityView
from app.views import ComparisonView

os.environ['ASYNC_TEST_TIMEOUT'] = '10'


class TestViews(AsyncHTTPTestCase):

    def get_app(self):

        return Application([(r"/city/([a-zA-Z0-9]*)?", CityView, {}),
                            (r"/comparison/cities=\[(\S+)\]", ComparisonView, {})])

    def test_city_view(self):
        """
        Get the location data for a city and check the
        :return:
        """
        for city_name in ['dublin', 'London', 'Copenhagen']:
            response = self.fetch(
                path="/city/{}".format(city_name),
                method='GET'
            )
            self.assertEqual(response.code, HTTPStatus.OK)
            print(response.body)
            self.check_city_response(response, city_name.title())

    def test_city_view_fails(self):
        """
        Try to get the location data for an unknown city. 
        Check to make sure that a NOT FOUND is returned.
        :return:
        """
        city_name = 'notarealplace'
        response = self.fetch(
            path="/city/{}".format(city_name),
            method='GET'
        )
        self.assertEqual(response.code,
                         HTTPStatus.NOT_FOUND,
                         "Incorrect response for an unknown city")

    def test_compare_cities(self):
        city_names = ['dublIn', 'london', 'lisbon', 'Amsterdam', 'CopenHaGeN']

        response = self.fetch(
            path="/comparison/cities=[{}]".format(','.join(city_names)),
            method='GET'
        )
        self.assertEqual(response.code, HTTPStatus.OK)
        print(response.body)
        self.check_comparison_response(response, city_names)

    #  Checks for responses #

    def check_city_response(self, response, city_name):
        """
        Checks that the response for an individual city contains the relevant information.
        :param response: The raw response from the route
        :param city_name: The name of the requested city
        :return:
        """
        body = json_decode(response.body)
        self.assertEqual(type(body), dict)
        self.assertIsNotNone(body.get('city_name'))
        self.assertIsNotNone(body.get('current_temperature'))
        self.assertIsNotNone(body.get('current_weather_description'))
        self.assertIsNotNone(body.get('population'))
        self.assertIsNotNone(body.get('bars'))

        # TODO: Reimplement this using a weighted decile
        # system as outlined in compare_cities
        #self.assertIsNotNone(body.get('city_score'))

        self.assertEqual(body.get('city_name'), city_name)

        self.assertIsInstance(body.get('current_temperature'), numbers.Number, "The current temperature is not numeric")
        self.assertIsInstance(body.get('current_weather_description'), str, "The weather description is not a string")
        self.assertIsInstance(body.get('population'), int, "The population is not an integer")
        self.assertIsInstance(body.get('bars'), int, "The number of bars is not an integer")
        # TODO: Reimplement this using a weighted decile
        # system as outlined in compare_cities
        # self.assertIsNotNone(body.get('city_score'))

        # self.assertIsInstance(body.get('city_score'), numbers.Number, "The city score is not a number")

    def check_comparison_response(self, response, city_names):
        """
        Various checks on the results of a comparison response. Checks include the correct fields as well as the
        ordering of the ranking.
        :param response: The raw response from the route
        :param city_names: A list if the city names used for comparison
        :return:
        """
        body = json_decode(response.body)

        # General checks for the correct information
        self.assertIsNotNone(body.get('city_data'))
        data = body.get('city_data')
        # Ensure the results contain each city name
        self.assertEqual(len(data), len(city_names), "Incorrect number of cities returned")
        self.assertTrue(set([x.title() for x in city_names]) <= {x.get('city_name').title() for x in data},
                        "All cities not included in the results")
        for city_result in data:
            self.assertIsInstance(city_result, dict, "Incorrect type for the city_data entries")

            # Confirm that the city name is included in the data
            expected = {'city_name'}
            self.assertTrue(expected <= set(city_result.keys()),
                            "Missing entries in a city_data entry")

            # Confirm that the City Name is a String
            self.assertIsInstance(city_result.get('city_name'), str, "Incorrect type of city_name")
