from unittest import TestCase
from app.compare_cities import compare_cities
from app.profiles import boozehound

class TestCompareCities(TestCase):
    def test_compare_cities(self):
        city_list = [
            {
                "city": "Dublin",
                "population": "482000",
                "bars": "868",
                "bars_per_capita": 0.001,
                "public_transport": "2"
            },
            {
                "city": "Copenhagen",
                "population": "499000",
                "bars": "998",
                "bars_per_capita": 0.002,
                "public_transport": "4"
            }
        ]
        expected = [
            {
                "city": "Copenhagen",
                "population": "499000",
                "bars": "998",
                "bars_per_capita": 0.002,
                "public_transport": "4"
            },
            {
                "city": "Dublin",
                "population": "482000",
                "bars": "868",
                "bars_per_capita": 0.001,
                "public_transport": "2"
            }
        ]
        self.assertListEqual(expected, compare_cities(city_list, boozehound))
