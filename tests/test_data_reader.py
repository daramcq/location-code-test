from unittest import TestCase
from collections import OrderedDict
from app.data_reader import find_city

class TestDataReader(TestCase):

    def test_find_city(self):
        expected = {
            'index': '1',
            'city': 'Moscow',
            'country': 'Russia',
            'population': '8297000',
            'bars': '1659',
            'museums': '1936',
            'crime_rate': '9',
            'public_transport': '4',
            'average_hotel_cost': '258'
        }

        city = dict(find_city('Moscow'))
        self.assertDictEqual(expected, city)

    def test_find_city_non_existent(self):
        with self.assertRaises(IndexError):
            find_city('Bogus')
