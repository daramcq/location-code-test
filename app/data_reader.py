import csv

CITY_DATA_FILE = 'data/european_cities.csv'

def normalised(s):
    return s.lower().strip()

def name_matches(city_entry, city_name):
    name_field = city_entry.get('city')
    return normalised(name_field) == normalised(city_name)

def find_city(city_name):
    with open(CITY_DATA_FILE, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for line in reader:
            if name_matches(line, city_name):
                return line
    raise IndexError

def read_all_cities():
    cities = []
    with open(CITY_DATA_FILE, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for line in reader:
            cities.append(line)
    return cities
