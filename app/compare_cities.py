from .data_reader import read_all_cities
from .weather_query import city_current_weather

bars_per_capita = lambda city: int(city['bars'])/float(city['population'])


def compare_cities(city_list, profile):
    """
    Takes a list of cities and sorts them according to
    their characteristics based on a model
    TODO: This should be replaced with a weighted decile ranking,
    which incorporates the assessed fields (decile-ranked versus
    the full dataset), weighted and integrated into a single score.
    """
    sorted_cities = sorted(city_list, key=profile.comparator, reverse=True)
    return sorted_cities


def enrich_city(city):
    """
    Enriches a city object with metrics to support comparison
    """
    city['bars_per_capita'] = bars_per_capita(city)
    current_weather = city_current_weather(city.get('city'))
    city['current_weather_description'] = current_weather['weather_state_name']
    city['current_temperature'] = current_weather['the_temp']
    return city
