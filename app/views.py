from tornado.web import RequestHandler
from http import HTTPStatus
import json

from .data_reader import find_city
from .compare_cities import compare_cities, enrich_city
from .profiles import boozehound


def digitalise(el):
    if isinstance(el, str) and el.isdigit():
        return int(el)
    else:
        return el


def normalise_city(city):
    city.pop('index')
    city['city_name'] = city.pop('city').title()
    for k, v in city.items():
        city[k] = digitalise(v)
    return city


def lookup_profile(profile_name):
    from app import profiles
    return getattr(profiles, profile_name)


class CityView(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    def get(self, *args, **kwargs):
        """
        Retrieve information about a city.
        :param args:
        :param kwargs:
        :return:
        """
        city_name = args[0]
        try:
            city = find_city(city_name)
            city = enrich_city(city)
            status = HTTPStatus.OK
            response = normalise_city(city)
        except IndexError:
            response = {
                'error': "City '{}' not found".format(city_name)
            }
            status = HTTPStatus.NOT_FOUND
        self.set_status(status)
        self.write(response)


def make_city_comparison(city_list, profile):
    try:
        cities = [find_city(city) for city in city_list]
        cities = [enrich_city(city) for city in cities]
    except (RequestException, IndexError):
        raise Exception(("Encountering service problems, "
                         "please try again later!"))
    cities = compare_cities(cities, profile)
    return cities

        
class ComparisonView(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    def get(self, *args, **kwargs):
        """
        Retrieves information about multiple cities, rates them and
        returns a ranking and score for each city.
        :param args:
        :param kwargs:
        :return:
        """
        try:
            cities_requested = args[0].split(',')
            if len(args) > 1:
                profile = args[1]
            else:
                profile = 'culturefiend'
            profile = lookup_profile(profile)
            cities = make_city_comparison(cities_requested, profile)
            response = {
                'city_data': [normalise_city(c) for c in cities],
                'info': profile.description
            }
            status = HTTPStatus.OK
        except IndexError:
            response = {
                'error': ("Unable to find data for cities '{}'."
                          "Please try again with different city names").format(', '.join(cities_requested))
            }
            status = HTTPStatus.BAD_REQUEST
        except Exception as e:
            response = {
                'error': str(e)
            }
            status = HTTPStatus.SERVICE_UNAVAILABLE

        self.set_status(status)
        self.write(response)
