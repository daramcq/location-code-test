from collections import namedtuple

profile = namedtuple('Profile', ['description', 'comparator'])

boozehound_desc = ("Boozehound ranks cities by the number of bars per person "
                   "and the public transport available.")
boozehound_cmp = lambda city: (city['bars_per_capita'] * 7
                               + float(city['public_transport'] * 3))
boozehound = profile(boozehound_desc, boozehound_cmp)


culturefiend_desc = ("CultureFiend wants lots of museums and great public transport "
                      "so they can cram in as many sights as possible.")                      
culturefiend_cmp = lambda city: ((city['museums'] * 6)
                             + (city['public_transport'] * 4))
culturefiend = profile(culturefiend_desc, culturefiend_cmp)

sunworshipper_desc = "SunWorshipper only wants good weather"
sunworshipper_cmp = lambda city: city['current_temperature']
sunworshipper = profile(sunworshipper_desc, sunworshipper_cmp)
