import requests


WEATHER_API = 'http://www.metaweather.com/api/'
LOCATION_SEARCH = WEATHER_API + 'location/search/?query={}'
FORECAST_ENDPOINT = WEATHER_API + 'location/{}'

def make_request(url):
    resp = requests.get(url)
    if resp.status_code != 200:
        err_msg = ("API Request {} failed with status code {}").format(url, resp.status_code)
        raise requests.exceptions.RequestException(err_msg)
    return resp.json()


def find_location(city_name):
    """
    Finds the woeid (unique location id)
    for a city
    TODO: Add caching to reduce calls
    """
    url = LOCATION_SEARCH.format(city_name)
    js = make_request(url)
    return js[0]['woeid']


def get_location_weather(woeid):
    url = FORECAST_ENDPOINT.format(woeid)
    js = make_request(url)
    return js.get('consolidated_weather')


def city_current_weather(city_name):
    location_id = find_location(city_name)
    forecast = get_location_weather(location_id)
    return forecast[0]
